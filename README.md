**Scrip de despliegue en terraform para clientes AWS**

Este script se basa en el despliegue la red de AWS para clientes en nuevas cuentas.

*Es necesario instalar AWS CLI para el uso de credenciales*

---

## Instalar AWS CLI

Es necesario installar AWS CLI para configurar un perfil y así terraform usará este mismo perfil para obtener las credenciales.

1. Descargue el instalador del archivo ejecutable de Python 3.6 para Windows x86-64 desde la página de descargas de Python.org..
2. Ejecute el instalador.
3. Elija Add Python 3.6 to PATH..
4. Seguir guia https://docs.aws.amazon.com/es_es/cli/latest/userguide/awscli-install-windows.html

---

## Crear un perfil en AWS CLI

El siguiente paso es ingresar al **CMD** para ejecutar **aws configure** donde se ingresará los parametros para la creación del perfil.

1. Abrir el CMD de windows.
2. Ejecutar aws configure.
3. Ingresar el access key y enter.
4. Ingresar el secret key y enter.
5. Dejar los demas parametros por defecto.

Lo que hará este comando es crear un archivo en el perfil del usuario **~/aws/credentials** y terraform consumirá estos datos para  el uso de permisos. 

---

## Clonar repositorio

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).