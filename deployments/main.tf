provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "~/.aws/credentials"
  profile                 = "default"

  assume_role {
    role_arn = "arn:aws:iam::${var.cuenta}:role/CrossAccountSeidorTech"

    #role_arn = "arn:aws:iam::116332599801:role/CrossAccountSeidorTech"
  }
}

module "vpc" {
  source = "networking/"

  name = "TERRAFORM"

  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a", "us-east-1b"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
  public_subnets  = ["10.0.3.0/24", "10.0.4.0/24"]

  tags = {
    Owner       = "user"
    Environment = "dev"
  }
}
